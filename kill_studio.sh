#!/bin/bash

if [ "$1" == "intellij" ]; then
  SEARCH="idea-IC"
else
  SEARCH="studio"
fi

OUTPUT=`ps ax | grep $SEARCH | grep -v grep | grep -v scripts/kill_studio.sh | awk '{print $1}'`

ps ax | grep $SEARCH | grep -v grep | grep -v scripts/kill_studio.sh | cut -c -80

read -p "Kill these processes? [Y/n] " -n 1 -r

echo ""

if [[ $REPLY =~ ^[Yy]$ ]]
then
  kill -9 $OUTPUT
fi

echo ""
